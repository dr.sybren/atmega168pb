#include "lcd_i2c_driver.h"
#include "i2cmaster.h"

#include <stdio.h>
#include <util/delay.h>

typedef struct LCD_I2C
{
    uint8_t i2c_addr;
    uint8_t lcd_cols;
    uint8_t lcd_rows;
    uint8_t charsize;

    uint8_t backlight;
    uint8_t displayfunction;
    uint8_t displaycontrol;
    uint8_t displaymode;
} LCD_I2C;

// For simplicity just have one global instance.
LCD_I2C lcd_i2c = {0};

/************ low level data pushing commands **********/

void _lcd_i2c_write(uint8_t data)
{
    i2c_start_wait(lcd_i2c.i2c_addr, I2C_WRITE);
    i2c_write(data | lcd_i2c.backlight);
    i2c_stop();
}

void _lcd_i2c_write4bits(uint8_t data)
{
    _lcd_i2c_write(data);      // Write the data
    _lcd_i2c_write(data | En); // 'E' pin of LCD high
    _delay_us(1);              // enable pulse must be >450ns
    _lcd_i2c_write(data);      // 'E' pin of LCD low
    _delay_us(50);             // commands need > 37us to settle
}

void _lcd_i2c_send_nibbled(uint8_t value, uint8_t control)
{
    uint8_t highnibble = value & 0xf0;
    uint8_t lownibble = (value << 4) & 0xf0;
    _lcd_i2c_write4bits(highnibble | control);
    _lcd_i2c_write4bits(lownibble | control);
}

void _lcd_i2c_command(uint8_t data)
{
    _lcd_i2c_send_nibbled(data, 0);
}

// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    DL = 1; 8-bit interface data
//    N = 0; 1-line display
//    F = 0; 5x8 dot character font
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidCrystal constructor is called).
void lcd_i2c_begin(uint8_t i2c_addr, uint8_t lcd_cols, uint8_t lcd_rows, uint8_t charsize)
{
    lcd_i2c.i2c_addr = i2c_addr;
    lcd_i2c.lcd_cols = lcd_cols;
    lcd_i2c.lcd_rows = lcd_rows;
    lcd_i2c.charsize = charsize;
    lcd_i2c.backlight = LCD_BACKLIGHT;

    lcd_i2c.displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
    if (lcd_rows > 1)
    {
        lcd_i2c.displayfunction |= LCD_2LINE;
    }

    // Initialize to default text direction (for roman languages)
    lcd_i2c.displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
    lcd_i2c.displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;

    // SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
    // according to datasheet, we need at least 40ms after power rises above 2.7V
    // before sending commands. Arduino can turn on way befer 4.5V so we'll wait 50
    _delay_ms(50);

    // Now we pull both RS and R/W low to begin commands
    _lcd_i2c_write(0); // reset I2C expander and turn backlight on
    _delay_ms(250);

    //put the LCD into 4 bit mode
    // this is according to the hitachi HD44780 datasheet
    // figure 24, pg 46

    // we start in 8bit mode, try to set 4 bit mode
    _lcd_i2c_write4bits(0x03 << 4);
    _delay_us(4500); // wait min 4.1ms

    // second try
    _lcd_i2c_write4bits(0x03 << 4);
    _delay_us(4500); // wait min 4.1ms

    // third go!
    _lcd_i2c_write4bits(0x03 << 4);
    _delay_us(150);

    // finally, set to 4-bit interface
    _lcd_i2c_write4bits(0x02 << 4);

    // set # lines, font size, etc.
    _lcd_i2c_command(LCD_FUNCTIONSET | lcd_i2c.displayfunction);

    // turn the display on with no cursor or blinking default
    lcd_i2c_display();

    // clear it off
    lcd_i2c_clear();

    // set the entry mode
    _lcd_i2c_command(LCD_ENTRYMODESET | lcd_i2c.displaymode);

    lcd_i2c_home();
}

/********** high level commands, for the user! */
void lcd_i2c_clear()
{
    _lcd_i2c_command(LCD_CLEARDISPLAY); // clear display, set cursor position to zero
    _delay_us(2000);                    // this command takes a long time!
}

void lcd_i2c_home()
{
    _lcd_i2c_command(LCD_RETURNHOME); // set cursor position to zero
    _delay_us(2000);                  // this command takes a long time!
}

void lcd_i2c_setCursor(uint8_t col, uint8_t row)
{
    int row_offsets[] = {0x00, 0x40, 0x14, 0x54};
    if (row > lcd_i2c.lcd_rows)
    {
        row = lcd_i2c.lcd_rows - 1; // we count rows starting w/0
    }
    _lcd_i2c_command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void lcd_i2c_noDisplay()
{
    lcd_i2c.displaycontrol &= ~LCD_DISPLAYON;
    _lcd_i2c_command(LCD_DISPLAYCONTROL | lcd_i2c.displaycontrol);
}
void lcd_i2c_display()
{
    lcd_i2c.displaycontrol |= LCD_DISPLAYON;
    _lcd_i2c_command(LCD_DISPLAYCONTROL | lcd_i2c.displaycontrol);
}

// Turns the underline cursor on/off
void lcd_i2c_noCursor()
{
    lcd_i2c.displaycontrol &= ~LCD_CURSORON;
    _lcd_i2c_command(LCD_DISPLAYCONTROL | lcd_i2c.displaycontrol);
}
void lcd_i2c_cursor()
{
    lcd_i2c.displaycontrol |= LCD_CURSORON;
    _lcd_i2c_command(LCD_DISPLAYCONTROL | lcd_i2c.displaycontrol);
}

// Turn on and off the blinking cursor
void lcd_i2c_noBlink()
{
    lcd_i2c.displaycontrol &= ~LCD_BLINKON;
    _lcd_i2c_command(LCD_DISPLAYCONTROL | lcd_i2c.displaycontrol);
}
void lcd_i2c_blink()
{
    lcd_i2c.displaycontrol |= LCD_BLINKON;
    _lcd_i2c_command(LCD_DISPLAYCONTROL | lcd_i2c.displaycontrol);
}

// These commands scroll the display without changing the RAM
void lcd_i2c_scrollDisplayLeft(void)
{
    _lcd_i2c_command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void lcd_i2c_scrollDisplayRight(void)
{
    _lcd_i2c_command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void lcd_i2c_leftToRight(void)
{
    lcd_i2c.displaymode |= LCD_ENTRYLEFT;
    _lcd_i2c_command(LCD_ENTRYMODESET | lcd_i2c.displaymode);
}

// This is for text that flows Right to Left
void lcd_i2c_rightToLeft(void)
{
    lcd_i2c.displaymode &= ~LCD_ENTRYLEFT;
    _lcd_i2c_command(LCD_ENTRYMODESET | lcd_i2c.displaymode);
}

// This will 'right justify' text from the cursor
void lcd_i2c_autoscroll(void)
{
    lcd_i2c.displaymode |= LCD_ENTRYSHIFTINCREMENT;
    _lcd_i2c_command(LCD_ENTRYMODESET | lcd_i2c.displaymode);
}

// This will 'left justify' text from the cursor
void lcd_i2c_noAutoscroll(void)
{
    lcd_i2c.displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
    _lcd_i2c_command(LCD_ENTRYMODESET | lcd_i2c.displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void lcd_i2c_createChar(uint8_t location, uint8_t charmap[])
{
    location &= 0x7; // we only have 8 locations 0-7
    _lcd_i2c_command(LCD_SETCGRAMADDR | (location << 3));
    for (int i = 0; i < 8; i++)
    {
        _lcd_i2c_write(charmap[i]);
    }
}

// Turn the (optional) backlight off/on
void lcd_i2c_noBacklight(void)
{
    lcd_i2c.backlight = LCD_NOBACKLIGHT;
    _lcd_i2c_write(0);
}

void lcd_i2c_backlight(void)
{
    lcd_i2c.backlight = LCD_BACKLIGHT;
    _lcd_i2c_write(0);
}

void lcd_i2c_text(const char * text) {
    for (; *text; text++) {
        _lcd_i2c_send_nibbled(*text, Rs);
    }
}

int lcd_i2c_printf(const char *format, ...) {
    char buffer[64];
    va_list var_args;

    va_start(var_args, format);
    int char_count = vsnprintf(buffer, sizeof(buffer), format, var_args);
    va_end(var_args);

    lcd_i2c_text(buffer);

    return char_count;
}
