#include "i2cmaster.h"
#include "lcd_i2c_driver.h"

int main()
{
    i2c_init();

    lcd_i2c_begin(0x27, 16, 2, LCD_5x8DOTS);
    lcd_i2c_printf("SCANNING...");
    lcd_i2c_setCursor(0, 1);

    for (uint8_t addr=0; addr < 128; addr++) {
        if (i2c_start(addr, I2C_WRITE)) continue;

        lcd_i2c_printf("%X ", addr);
    }
}
