/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#include "setup.h"
#include "i2cmaster.h"
#include "mcp9808_driver.h"
#include "sevenseg.h"

#include <stdbool.h>

#include <avr/io.h>

#define MCP_I2C_ADDR 0x18

static void setup_timer1()
{
  // Timer 1 for periodic temperature updates:
  // WGM 0b0100 for CTC mode.
  TCCR1A = 0                  //
           | (0b00 << WGM10)  // CTC mode
      ;
  TCCR1B = 0                  //
           | (0b01 << WGM12)  // CTC mode
           | (0b101)          // clk / 1024
      ;
  OCR1A = DEFAULT_LOW_FREQ_COUNTER;  // ~2 Hz interrupts
  TIFR1 = 0;                         // Clear all interrupt flags
  TIMSK1 = (1 << OCIE1A);            // Enable Interrupts
}

void setup()
{
  // Always disable watchdog reset at startup.
  MCUSR &= ~(1 << WDRF);
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0;

  // Set the expected clockdiv.
  CLKPR = 1 << CLKPCE;
#if F_CPU == 8000000
  CLKPR = 0b0000;
#elif F_CPU == 1000000
  CLKPR = 0b0011;
#else
#  error Unexpected value for F_CPU
#endif

  PCICR = 0;  // Disable PCINT except the ones we configure.

  GTCCR = 0;  // Disable any timer sync at boot.

  PRR = 0                  //
        | (1 << PRSPI)     // SPI bus
        | (1 << PRTIM2)    // Timer 2
        | (1 << PRUSART0)  // USART
        | (1 << PRADC)     // ADC
      ;

  sevenseg_setup();
  MCP9808_temp startup_display = {
      .ta_vs_tcrit = true,
      .ta_vs_tupper = true,
      .ta_vs_tlower = true,
      .temperature = 88.88,
  };
  sevenseg_display_temp(startup_display);

  // Set PB6 / PCINT6 as pulled-up input for pushbutton:
  DDRB &= ~(1 << DDRB6);
  PORTB |= (1 << PORTB6);
  PCMSK0 |= (1 << PCINT6);
  PCICR |= (1 << PCIE0);

  // Configure temp sensor:
  i2c_init();
  mcp9808_init(MCP_I2C_ADDR);

  MCP9808_config config = mcp9808_get_config();
  config.bits.t_hysteresis = MCP9808_HYST_1_5;

  config.bits.alert_mode = false;      // false = comparator output; true = interrupt
  config.bits.alert_polarity = false;  // active low
  config.bits.alert_select = false;    // not-critical alert output
  config.bits.alert_control = true;    // enable alert pin
  config.bits.interrupt_clear = true;  // clear any pending interrupt
  config.bits.shutdown = false;
  mcp9808_set_config(config);
  mcp9808_set_resolution(MCP9808_RES_0_0625);
  mcp9808_set_limit(MCP9808_REG_TCRIT, 50.0);
  mcp9808_set_limit(MCP9808_REG_TUPPER, 50.0);
  mcp9808_set_limit(MCP9808_REG_TLOWER, 0.0);

  setup_timer1();

  // Set PC3 / PCINT11 as pulled-up input for temp alert:
  DDRC &= ~(1 << DDRC3);
  PORTC |= (1 << PORTC3);
  // PCMSK1 |= (1 << PCINT11);
  // PCICR |= (1 << PCIE1);

  // Wait for all the pull-up resistors to do their work.
  while ((PINB & (1 << PINB6)) == 0)
    ;
  while ((PINC & (1 << PINC3)) == 0)
    ;
}
