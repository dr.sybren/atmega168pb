#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=i2cmaster.c main.c mcp9808_driver.c setup.c sevenseg.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/i2cmaster.o ${OBJECTDIR}/main.o ${OBJECTDIR}/mcp9808_driver.o ${OBJECTDIR}/setup.o ${OBJECTDIR}/sevenseg.o
POSSIBLE_DEPFILES=${OBJECTDIR}/i2cmaster.o.d ${OBJECTDIR}/main.o.d ${OBJECTDIR}/mcp9808_driver.o.d ${OBJECTDIR}/setup.o.d ${OBJECTDIR}/sevenseg.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/i2cmaster.o ${OBJECTDIR}/main.o ${OBJECTDIR}/mcp9808_driver.o ${OBJECTDIR}/setup.o ${OBJECTDIR}/sevenseg.o

# Source Files
SOURCEFILES=i2cmaster.c main.c mcp9808_driver.c setup.c sevenseg.c

# Pack Options 
PACK_COMPILER_OPTIONS=-I "${DFP_DIR}/include"
PACK_COMMON_OPTIONS=-B "${DFP_DIR}/gcc/dev/atmega168pb"



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=ATmega168PB
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/i2cmaster.o: i2cmaster.c  .generated_files/c2b924c4a6c276c3193d16888e8a22d693a81086.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/i2cmaster.o.d 
	@${RM} ${OBJECTDIR}/i2cmaster.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/i2cmaster.o.d" -MT "${OBJECTDIR}/i2cmaster.o.d" -MT ${OBJECTDIR}/i2cmaster.o  -o ${OBJECTDIR}/i2cmaster.o i2cmaster.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/main.o: main.c  .generated_files/25912b7f6f7100899f1d466c31719bfa5c0a641.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/main.o.d" -MT "${OBJECTDIR}/main.o.d" -MT ${OBJECTDIR}/main.o  -o ${OBJECTDIR}/main.o main.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/mcp9808_driver.o: mcp9808_driver.c  .generated_files/701f514649ae538593cad0c0d74b9800bf1a74e8.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/mcp9808_driver.o.d 
	@${RM} ${OBJECTDIR}/mcp9808_driver.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/mcp9808_driver.o.d" -MT "${OBJECTDIR}/mcp9808_driver.o.d" -MT ${OBJECTDIR}/mcp9808_driver.o  -o ${OBJECTDIR}/mcp9808_driver.o mcp9808_driver.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/setup.o: setup.c  .generated_files/8e7e39915e34b943d4b0e1733a065addd8223106.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/setup.o.d 
	@${RM} ${OBJECTDIR}/setup.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/setup.o.d" -MT "${OBJECTDIR}/setup.o.d" -MT ${OBJECTDIR}/setup.o  -o ${OBJECTDIR}/setup.o setup.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/sevenseg.o: sevenseg.c  .generated_files/75583a15f6f96ed2732fcb0f2b13d0870d196702.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sevenseg.o.d 
	@${RM} ${OBJECTDIR}/sevenseg.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/sevenseg.o.d" -MT "${OBJECTDIR}/sevenseg.o.d" -MT ${OBJECTDIR}/sevenseg.o  -o ${OBJECTDIR}/sevenseg.o sevenseg.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/i2cmaster.o: i2cmaster.c  .generated_files/fc38801d00c23a90efeb42d6a25bfcbcd46b40fd.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/i2cmaster.o.d 
	@${RM} ${OBJECTDIR}/i2cmaster.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/i2cmaster.o.d" -MT "${OBJECTDIR}/i2cmaster.o.d" -MT ${OBJECTDIR}/i2cmaster.o  -o ${OBJECTDIR}/i2cmaster.o i2cmaster.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/main.o: main.c  .generated_files/669d77aa1127f31cd93fd682a63cfc92b2c905b0.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/main.o.d" -MT "${OBJECTDIR}/main.o.d" -MT ${OBJECTDIR}/main.o  -o ${OBJECTDIR}/main.o main.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/mcp9808_driver.o: mcp9808_driver.c  .generated_files/d369357e3ddd2deacbf2ff6c7e505b315e478835.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/mcp9808_driver.o.d 
	@${RM} ${OBJECTDIR}/mcp9808_driver.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/mcp9808_driver.o.d" -MT "${OBJECTDIR}/mcp9808_driver.o.d" -MT ${OBJECTDIR}/mcp9808_driver.o  -o ${OBJECTDIR}/mcp9808_driver.o mcp9808_driver.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/setup.o: setup.c  .generated_files/62496b6e49a4e5d73e9b36903e0f49b3c20917e1.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/setup.o.d 
	@${RM} ${OBJECTDIR}/setup.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/setup.o.d" -MT "${OBJECTDIR}/setup.o.d" -MT ${OBJECTDIR}/setup.o  -o ${OBJECTDIR}/setup.o setup.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/sevenseg.o: sevenseg.c  .generated_files/c767355acb34b6d3de08ea7c52c9df144da3b7a6.flag .generated_files/8ed0d214b76bce2f2a4b891148bec3ca9cbaffae.flag
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sevenseg.o.d 
	@${RM} ${OBJECTDIR}/sevenseg.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega168pb ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/sevenseg.o.d" -MT "${OBJECTDIR}/sevenseg.o.d" -MT ${OBJECTDIR}/sevenseg.o  -o ${OBJECTDIR}/sevenseg.o sevenseg.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atmega168pb ${PACK_COMMON_OPTIONS}   -gdwarf-2 -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist\${CND_CONF}\${IMAGE_TYPE}\thermometer.${IMAGE_TYPE}.map"    -o dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1 -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,--end-group 
	
	
	
	
	
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atmega168pb ${PACK_COMMON_OPTIONS}  -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist\${CND_CONF}\${IMAGE_TYPE}\thermometer.${IMAGE_TYPE}.map"    -o dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}      -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION) -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,--end-group 
	${MP_CC_DIR}\\avr-objcopy -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/thermometer.${IMAGE_TYPE}.hex"
	
	
	
	
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
