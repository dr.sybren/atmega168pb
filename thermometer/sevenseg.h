/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#pragma once

#include "mcp9808_driver.h"
#include <stdint.h>

void sevenseg_setup();
void sevenseg_display(float number);
void sevenseg_display_temp(MCP9808_temp temperature);
void sevenseg_extra_dots(bool leftmost, bool rightmost);
void sevenseg_left_dot(bool enable);
void sevenseg_right_dot(bool enable);

void sevenseg_on();
void sevenseg_off();
void sevenseg_toggle();
void sevenseg_set_dim_skip(uint8_t new_dimming_skips);
