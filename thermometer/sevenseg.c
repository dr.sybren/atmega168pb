/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#include "sevenseg.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#define abs(x) ((x) < 0 ? -(x) : (x))

// Bit order of the segments is: GCpDEBFA
#define G (1 << 0)
#define C (1 << 1)
#define DP (1 << 2)
#define D (1 << 3)
#define E (1 << 4)
#define B (1 << 5)
#define F (1 << 6)
#define A (1 << 7)
const uint8_t PORTD_SECTIONS[] = {
    A | B | C | D | E | F,      // 0
    B | C,                      // 1
    A | B | D | E | G,          // 2
    A | B | C | D | G,          // 3
    B | C | F | G,              // 4
    A | F | G | C | D,          // 5
    A | C | D | E | F | G,      // 6
    A | B | C,                  // 7
    A | B | C | D | E | F | G,  // 8
    A | B | C | D | F | G,      // 9

};
const uint8_t PORTD_SECTION_DP = DP;

// Pins used by the LED segments
#define PORTB_MASK (1 << PORTB0)
#define PORTC_MASK ((1 << PORTC1) | (1 << PORTC0))
#define PORTE_MASK (1 << PORTE3)

typedef int16_t CentiDegree;
static volatile CentiDegree display_number = 0;      // in 1/100 degree C
static volatile CentiDegree display_number_abs = 0;  // in 1/100 degree C
static volatile uint8_t decimal_point_pos = 1;
static volatile uint8_t display_dot_mask = 0;
static volatile bool display_dot_leftmost = false;
static volatile bool display_dot_rightmost = false;
static volatile bool display_enabled = true;
static volatile uint8_t dimming_skips = 0;  // Number of timer interrupts to just blank the LEDs.

static void select_digit(uint8_t digit_index)
{
  switch (digit_index) {
    case 0:
      PORTB &= ~PORTB_MASK;
      PORTC = (PORTC & ~PORTC_MASK) | (1 << PORTC1);  // 1
      PORTE &= ~PORTE_MASK;
      break;
    case 1:
      PORTB &= ~PORTB_MASK;
      PORTC = (PORTC & ~PORTC_MASK) | (1 << PORTC0);  // 2
      PORTE &= ~PORTE_MASK;
      break;
    case 2:
      PORTB &= ~PORTB_MASK;
      PORTC &= ~PORTC_MASK;
      PORTE = (PORTE & ~PORTE_MASK) | (1 << PORTE3);  // 3
      break;
    case 3:
      PORTB = (PORTB & ~PORTB_MASK) | (1 << PORTB0);  // 4
      PORTC &= ~PORTC_MASK;
      PORTE &= ~PORTE_MASK;
      break;
  }
}

static uint8_t segments_for_digit(uint8_t digit_index)
{
  uint8_t digit = 0;

  switch (digit_index) {
    case 0:
      digit = display_number_abs % 10;
      break;
    case 1:
      digit = (display_number_abs / 10) % 10;
      break;
    case 2:
      digit = (display_number_abs / 100) % 10;
      break;
    case 3:
      if (display_number < 0) {
        return G;
      }
      if (display_number < 1000) {
        return 0;
      }
      digit = (display_number_abs / 1000) % 10;
      break;
  }

  uint8_t segments = PORTD_SECTIONS[digit];
  if (display_dot_mask & (1 << digit_index)) {
    segments |= DP;
  }
  return segments;
}

ISR(TIMER0_COMPA_vect)
{
  static uint8_t skip_count = 0;
  static uint8_t digit_index = 255;

  PORTD = 255;

  if (dimming_skips) {
    skip_count = (skip_count + 1) % dimming_skips;
    if (skip_count) {
      return;
    }
  }

  // Display one digit at a time.
  digit_index = (digit_index + 1) & 0b11;
  select_digit(digit_index);

  // Pull down on PORTD pins for the segment to show.
  uint8_t positive = segments_for_digit(digit_index);
  PORTD = ~positive;
}

void sevenseg_setup()
{
  DDRB = (1 << DDB0);
  PORTB = 0;
  DDRC = (1 << DDC1) | (1 << DDC0);
  PORTC = 0;
  DDRE = (1 << DDE3);
  PORTE = 0;

  DDRD = 255;
  PORTD = 255;

  // Timer for LED segments updates:
  TCCR0A = 0b10 << WGM00;  // CTC mode
  TCCR0B = 0b010 << CS00;  // clkdiv
  OCR0A = 255;
  TIFR0 = 0;  // clear all interrupt flags
  TIMSK0 = 1 << OCIE0A;
}

static void _update_display_dot_mask()
{
  display_dot_mask = 0                             //
                     | display_dot_leftmost << 3   //
                     | 1 << decimal_point_pos      //
                     | display_dot_rightmost << 0  //
      ;
}

void sevenseg_display(float number)
{
  if (number <= -10 || number >= 100.0f) {
    display_number = number * 10;
    decimal_point_pos = 1;
  }
  else {
    display_number = number * 100;
    decimal_point_pos = 2;
  }
  display_number_abs = abs(display_number);
  _update_display_dot_mask();
}

void sevenseg_display_temp(MCP9808_temp temperature)
{
  sevenseg_display(temperature.temperature);
}

void sevenseg_extra_dots(bool leftmost, bool rightmost)
{
  display_dot_leftmost = leftmost;
  display_dot_rightmost = rightmost;
  _update_display_dot_mask();
}

void sevenseg_left_dot(bool enable)
{
  display_dot_leftmost = enable;
  _update_display_dot_mask();
}

void sevenseg_right_dot(bool enable)
{
  display_dot_rightmost = enable;
  _update_display_dot_mask();
}

void sevenseg_on()
{
  display_enabled = true;
  TIMSK0 |= (1 << OCIE0A);
  PRR &= ~(1 << PRTIM0);
  PORTD = 255;
}

void sevenseg_off()
{
  display_enabled = false;
  PRR |= (1 << PRTIM0);
  TIMSK0 &= ~(1 << OCIE0A);

  PORTB &= ~(1 << DDB0);
  PORTC &= ~((1 << DDC1) | (1 << DDC0));
  PORTE &= ~(1 << DDE3);
  PORTD = 0;
}

void sevenseg_toggle()
{
  if (display_enabled) {
    sevenseg_off();
  }
  else {
    sevenseg_on();
  }
}

void sevenseg_set_dim_skip(const uint8_t new_dimming_skips)
{
  dimming_skips = new_dimming_skips;
}
