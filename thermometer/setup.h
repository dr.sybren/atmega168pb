/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#pragma once

// 31248 = ~1/4 Hz
// 15624 = ~1/2 Hz
// 7812 = ~1 Hz
// 3906 = ~2 Hz
// 1953 = ~4 Hz
//  488 = ~8 Hz
#define DEFAULT_LOW_FREQ_COUNTER 31248
#define DEFAULT_HYPER_FREQ_COUNTER 3906

#define DEFAULT_LOW_FREQ_DIM_SKIP 8
#define DEFAULT_HYPER_FREQ_DIM_SKIP 0

#define F_CPU 8000000

void setup(void);
