/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#include "mcp9808_driver.h"
#include "setup.h"
#include "sevenseg.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <util/delay.h>

volatile bool interrupt_temperature_update = false;
volatile bool interrupt_pushbutton = false;
volatile bool hyper_mode = false;

float smooth_temp = INFINITY;

static void set_hyper_mode(const bool new_hyper_mode)
{
  if (new_hyper_mode) {
    OCR1A = DEFAULT_HYPER_FREQ_COUNTER;
    sevenseg_set_dim_skip(DEFAULT_HYPER_FREQ_DIM_SKIP);
  }
  else {
    OCR1A = DEFAULT_LOW_FREQ_COUNTER;
    sevenseg_set_dim_skip(DEFAULT_LOW_FREQ_DIM_SKIP);
  }
  TCNT1 = OCR1A - (OCR1A >> 2);

  hyper_mode = new_hyper_mode;
}

static void maybe_smooth_temperature(MCP9808_temp *temp)
{
  const float diff = temp->temperature - smooth_temp;
  const float absdiff = fabs(diff);

  if (absdiff < 1.0f) {
    const float alpha = hyper_mode ? 0.1f : 0.2f;
    smooth_temp += diff * alpha;
  }
  else {
    smooth_temp = temp->temperature;
  }

  temp->temperature = smooth_temp;
}

static void sample()
{
  sevenseg_right_dot(true);

  mcp9808_wakeup();
  _delay_ms(300);  // depends on temp resolution
  MCP9808_temp temp = mcp9808_read_temperature();
  mcp9808_shutdown();

  maybe_smooth_temperature(&temp);

  sevenseg_display_temp(temp);
  sevenseg_right_dot(false);
}

static void sample_and_show()
{
  sample();
  sevenseg_on();
}

ISR(TIMER1_COMPA_vect)
{
  interrupt_temperature_update = true;
}

static inline bool is_pushbutton_pressed()
{
  return (PINB & (1 << PB6)) == 0;
}

// Pushbutton changed value
ISR(PCINT0_vect)
{
  if (!is_pushbutton_pressed()) {
    return;
  }

  // Make up for the lack of hardware debouncing.
  for (uint16_t delay = 0; delay < 0x3FF; delay++)
    __asm__ __volatile__("");

  interrupt_pushbutton |= is_pushbutton_pressed();
}

static inline bool is_alert(void)
{
  return (PINC & (1 << PINC3)) == 0;
}

static void temperature_update()
{
  interrupt_temperature_update = false;
  sample_and_show();
}

static void pushbutton_pressed()
{
  interrupt_pushbutton = false;
  set_hyper_mode(!hyper_mode);
}

int main()
{
  setup();
  sei();

  set_hyper_mode(false);
  temperature_update();

  for (;;) {
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();

    if (interrupt_pushbutton)
      pushbutton_pressed();

    if (interrupt_temperature_update)
      temperature_update();
  }
}
