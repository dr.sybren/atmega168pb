/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#include "countdown.h"
#include "buzzer.h"
#include "sevenseg.h"
#include "states.h"

#include <stdbool.h>

#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

#define DEFAULT_MIN 7
#define DEFAULT_SEC 0

volatile int8_t countdown_minutes = DEFAULT_MIN;
volatile int8_t countdown_seconds = DEFAULT_SEC;
volatile static uint8_t clkdiv = 0;

const uint8_t EEPROM_INITIALISED = 47;
uint8_t *const EEPROM_OFFSET_INITIALISED = 0;
uint8_t *const EEPROM_OFFSET_COUNTDOWN_MINUTES = (uint8_t *)(1);
uint8_t *const EEPROM_OFFSET_COUNTDOWN_SECONDS = (uint8_t *)(2);

static void countdown_set_atomic(int8_t minutes, int8_t seconds)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    countdown_minutes = minutes;
    countdown_seconds = seconds;
    clkdiv = 0;
  }
}

static void countdown_get_atomic(int8_t *minutes, int8_t *seconds)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    *minutes = countdown_minutes;
    *seconds = countdown_seconds;
  }
}

static void countdown_save(int8_t minutes, int8_t seconds)
{
  eeprom_update_byte(EEPROM_OFFSET_INITIALISED, EEPROM_INITIALISED);
  eeprom_update_byte(EEPROM_OFFSET_COUNTDOWN_MINUTES, minutes);
  eeprom_update_byte(EEPROM_OFFSET_COUNTDOWN_SECONDS, seconds);
  countdown_set_atomic(minutes, seconds);
}

static void countdown_load()
{
  const int8_t inited = eeprom_read_byte(EEPROM_OFFSET_INITIALISED);
  const int8_t minutes = eeprom_read_byte(EEPROM_OFFSET_COUNTDOWN_MINUTES);
  const int8_t seconds = eeprom_read_byte(EEPROM_OFFSET_COUNTDOWN_SECONDS);

  const bool minutes_trustworthy = 0 <= minutes && minutes <= 100;
  const bool seconds_trustworthy = 0 <= seconds && seconds <= 59;

  if (inited != EEPROM_INITIALISED || !minutes_trustworthy || !seconds_trustworthy) {
    countdown_set_atomic(DEFAULT_MIN, DEFAULT_SEC);
  }
  else {
    countdown_set_atomic(minutes, seconds);
  }
}

void countdown_setup()
{
  // Timer 0 for countdown timer
  // WGM = 0b010 for CTC mode
  TCCR0A = 0                  //
           | (0b10 << WGM00)  // CTC
      ;
  TCCR0B = 0                  //
           | (0b101 << CS00)  // clk/1024 = 18 KHz
      ;
  OCR0A = 180;  // Interrupt at 100 Hz.
  countdown_reset();
}

void countdown_start()
{
  TIMSK0 |= (1 << OCIE0A);
}

void countdown_reset()
{
  countdown_pause();
  countdown_load();
}

void countdown_pause()
{
  TIMSK0 &= ~(1 << OCIE0A);
}

void countdown_increase()
{
  int8_t minutes, seconds;
  countdown_get_atomic(&minutes, &seconds);

  if (minutes >= 60) {
    seconds = 0;
    countdown_save(minutes, seconds);
    return;
  }

  seconds += 15;
  while (seconds >= 60) {
    ++minutes;
    seconds -= 60;
  }
  countdown_save(minutes, seconds);
}

void countdown_decrease()
{
  int8_t minutes, seconds;
  countdown_get_atomic(&minutes, &seconds);

  if (minutes < 1 && seconds < 15) {
    minutes = seconds = 0;
    countdown_save(minutes, seconds);
    return;
  }

  seconds -= 15;
  while (seconds < 0) {
    --minutes;
    seconds += 60;
  }
  countdown_save(minutes, seconds);
}

ISR(TIMER0_COMPA_vect)
{
  if (clkdiv++ < 99) {
    return;
  }
  clkdiv = 0;

  if (countdown_seconds == 0) {
    countdown_minutes--;
    countdown_seconds = 59;
  }
  else {
    countdown_seconds--;
  }

  if (countdown_minutes == 0 && countdown_seconds == 0) {
    // Timer done!
    state_goto_finished();
  }
}
