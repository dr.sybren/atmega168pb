/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#include "leds.h"

#include <avr/io.h>

void led_status(bool enable)
{
  if (enable) {
    PORTB |= (1 << PORTB5);
  }
  else {
    PORTB &= ~(1 << PORTB5);
  }
}

void led_uv(bool enable)
{
  if (enable) {
    PORTB |= (1 << PORTB2);
  }
  else {
    PORTB &= ~(1 << PORTB2);
  }
}

void led_white(bool enable)
{
  if (enable) {
    PORTB |= (1 << PORTB1);
  }
  else {
    PORTB &= ~(1 << PORTB1);
  }
}
