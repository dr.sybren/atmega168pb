/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#include "buzzer.h"

#include <stdbool.h>

#include <avr/interrupt.h>
#include <avr/io.h>

/* Buzzer is on PB3 / MOSI / OC2A */

void buzzer_setup()
{
  // Timer 1 for buzzer PWM
  // WGM = 0b0100 for CTC mode
  TCCR1A = 0                  //
           | (0b00 << WGM10)  // CTC
      ;
  TCCR1B = 0                  //
           | (0b01 << WGM12)  // CTC
           | (0b101 << CS10)  // clkdiv / 1024 = 18 KHz
      ;
  TCCR1C = 0;
  OCR1A = 20;
  buzzer_off();
}

ISR(TIMER1_COMPA_vect)
{
  static bool buzz;
  if (buzz) {
    PORTB |= (1 << PORTB3);
  }
  else {
    PORTB &= ~(1 << PORTB3);
  }
  buzz = !buzz;
}

void buzzer_on()
{
  TIMSK1 |= (1 << OCIE1A);
}

void buzzer_off()
{
  TIMSK1 &= ~(1 << OCIE1A);
  PORTB &= ~(1 << PORTB3);
}
