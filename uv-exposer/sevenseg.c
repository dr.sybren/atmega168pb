/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#include <math.h>
#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

// Bit order of the segments in PORTD is: GCpDEBFA
#define G (1 << 0)
#define C (1 << 1)
#define DP (1 << 2)
#define D (1 << 3)
#define E (1 << 4)
#define B (1 << 5)
#define F (1 << 6)
#define A (1 << 7)
const uint8_t PORTD_SECTIONS[] = {
    A | B | C | D | E | F,      // 0
    B | C,                      // 1
    A | B | D | E | G,          // 2
    A | B | C | D | G,          // 3
    B | C | F | G,              // 4
    A | F | G | C | D,          // 5
    A | C | D | E | F | G,      // 6
    A | B | C,                  // 7
    A | B | C | D | E | F | G,  // 8
    A | B | C | D | F | G,      // 9

};
const uint8_t PORTD_SECTION_DP = DP;
#undef G
#undef C
#undef DP
#undef D
#undef E
#undef B
#undef F
#undef A

// Pins NOT used by the LED segments
#define PORTC_MASK 0b11111000

static volatile uint8_t display_minutes = 8;
static volatile uint8_t display_seconds = 59;
static volatile bool minutes_single_digit = true;
static volatile bool display_enabled = true;

static inline void select_digit(uint8_t digit_index)
{
  // Prevent showing the previous segment in the next digit.
  PORTD = 255;

  // Enable the desired digit.
  PORTC = (PORTC & PORTC_MASK) | (1 << digit_index);
}

static inline uint8_t segments_for_digit(uint8_t digit_index)
{
  uint8_t digit = 0;
  bool show_dp = false;

  if (minutes_single_digit) {
    switch (digit_index) {
      case 0:
        digit = display_seconds;
        break;
      case 1:
        digit = display_seconds / 10;
        break;
      case 2:
        show_dp = true;
        digit = display_minutes;
        break;
    }
  }
  else {
    switch (digit_index) {
      case 0:
        digit = display_seconds / 10;
        break;
      case 1:
        show_dp = true;
        digit = display_minutes;
        break;
      case 2:
        digit = display_minutes / 10;
        break;
    }
  }

  digit %= 10;
  uint8_t segments = PORTD_SECTIONS[digit];
  if (show_dp) {
    segments |= PORTD_SECTION_DP;
  }
  return segments;
}

// Timer 2, LED segments updates.
ISR(TIMER2_COMPA_vect)
{
  static uint8_t digit_index = 255;
  digit_index++;
  digit_index %= 3;

  uint8_t positive = segments_for_digit(digit_index);
  // uint8_t positive = PORTD_SECTION_DP;

  // // Swap digits as late as possible to avoid dark time.
  select_digit(digit_index);

  // Pull down on PORTD pins for the segment to show.
  PORTD = ~positive;
}

void sevenseg_setup()
{
  DDRC |= 0               //
          | (1 << DDRC0)  // Digit Select
          | (1 << DDRC1)  // Digit Select
          | (1 << DDRC2)  // Digit Select
      ;
  PORTC &= PORTC_MASK;

  // Timer 2 for LED segments updates:
  TCCR2A = 0b10 << WGM20;  // CTC mode
  TCCR2B = 0b111 << CS20;  // clkdiv / 1024 = 18 KHz
  TIMSK2 |= 1 << OCIE2A;   // Enable Interrupt
}

void sevenseg_display(int8_t minutes, int8_t seconds)
{
  display_minutes = minutes;
  display_seconds = seconds;
  minutes_single_digit = minutes < 10;
}

void sevenseg_on()
{
  TIMSK2 |= 1 << OCIE2A;  // Enable Interrupt
  display_enabled = true;
}

void sevenseg_off()
{
  TIMSK2 &= ~(1 << OCIE2A);  // Disable Interrupt
  PORTD = 255;
  display_enabled = false;
}

void sevenseg_toggle()
{
  if (display_enabled) {
    sevenseg_off();
  }
  else {
    sevenseg_off();
  }
}
