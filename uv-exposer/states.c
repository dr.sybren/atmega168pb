/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */

#include "states.h"
#include "buzzer.h"
#include "countdown.h"
#include "leds.h"

#include <stdbool.h>

volatile eSystemState system_state = STATE_DEFAULT;

void state_goto_default(void)
{
  system_state = STATE_DEFAULT;
  countdown_reset();
  led_status(false);
  led_uv(false);
  led_white(true);
  buzzer_off();
}

void state_goto_exposing(void)
{
  system_state = STATE_EXPOSING;
  countdown_start();
  led_white(false);
  led_status(true);
  led_uv(true);
  buzzer_off();
}

void state_goto_paused(void)
{
  system_state = STATE_PAUSED;
  countdown_pause();
  led_status(false);
  led_uv(false);
  led_white(true);
  buzzer_off();
}

void state_goto_finished(void)
{
  system_state = STATE_FINISHED;
  countdown_pause();
  led_status(false);
  led_uv(false);
  led_white(true);
  buzzer_on();
}
