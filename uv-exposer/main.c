/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2020 Sybren A. Stüvel.
 */
#include "buzzer.h"
#include "countdown.h"
#include "leds.h"
#include "sevenseg.h"
#include "states.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>

#define DDR_IN 0
#define DDR_OUT 1

// PINOUT ========================
//
// INPUTS:
// PB0: Pushbutton             PCINT0
// PC3: Rotary Encoder A       PCINT11
// PC4: Rotary Encoder B       PCINT12
// PC5: Rotary Encoder Click   PCINT13
//
// OUTPUTS:
// PB1: White LEDs
// PB2: UV LEDs
// PB3: MOSI & Buzzer
// PB4: MISO
// PB5: SCK  & Status LED
// PC0-2: Digit selection (source)
// PD0-7: Segment selection (sink)

volatile int8_t rotenc_ticks = 0;

void setup()
{
  // Always disable watchdog reset at startup.
  MCUSR &= ~(1 << WDRF);
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0;

  // Turn off components we don't use.
  PRR = 0                  //
        | (1 << PRTWI)     // I2C
        | (1 << PRSPI)     // SPI bus
        | (1 << PRUSART0)  // USART
        | (1 << PRADC)     // ADC
      ;

  // Configure I/O ports:
  DDRB = 0                     //
         | (DDR_OUT << DDRB1)  // White LEDs
         | (DDR_OUT << DDRB2)  // UV LEDs
         | (DDR_OUT << DDRB3)  // Buzzer
         | (DDR_OUT << DDRB5)  // Status LED
      ;
  DDRC = 0                     //
         | (DDR_OUT << DDRC0)  // Digit Select
         | (DDR_OUT << DDRC1)  // Digit Select
         | (DDR_OUT << DDRC2)  // Digit Select
      ;
  DDRD = 255;
  DDRE = 0;

  // Default inputs to pull-up and outputs to low:
  PORTB = ~DDRB;
  PORTC = ~DDRC;
  PORTD = ~DDRD;
  PORTE = ~DDRE;

  buzzer_setup();
  sevenseg_setup();
  sevenseg_display(47, 0);
  countdown_setup();

  // Wait around for a bit for the pull-ups to do their work.
  for (uint16_t i = 0; i < 65530; ++i) {
    __asm__ __volatile__("nop");
  }

  // Configure pin change interrupts:
  PCICR = 0               //
          | (1 << PCIE0)  // for PCINT[0:7]
          | (1 << PCIE1)  // for PCINT[14:8]
      ;
  PCMSK0 = (1 << PCINT0);    // PB0
  PCMSK1 = 0                 //
           | (1 << PCINT11)  // PC3
           | (1 << PCINT13)  // PC5
      ;

  state_goto_default();
}

ISR(PCINT0_vect)
{
  if (PINB & (1 << PB0)) {
    // Button is up, that's boring.
    return;
  }
  state_goto_default();
}

ISR(PCINT1_vect)
{
  // Rotary Encoder changed state.

  if ((PINC & (1 << PC5)) == 0) {
    // Clicker pushed
    switch (system_state) {
      case STATE_DEFAULT:
      case STATE_PAUSED:
        state_goto_exposing();
        break;
      case STATE_EXPOSING:
        state_goto_paused();
        break;
      case STATE_FINISHED:
        state_goto_default();
        break;
    }
    return;
  }

  if (system_state == STATE_EXPOSING) {
    // No response to rotary encoder while we're counting down.
    return;
  }

  uint8_t pinA = (PINC & (1 << PC3));
  uint8_t pinB = (PINC & (1 << PC4));
  if (pinA == 0) {
    if (pinB == 0) {
      // Turned left.
      if (rotenc_ticks < 100)
        ++rotenc_ticks;
    }
    else {
      // Turned right
      if (rotenc_ticks > -100)
        --rotenc_ticks;
    }
  }
}

void loop()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();

  while (rotenc_ticks > 0) {
    countdown_increase();
    --rotenc_ticks;
  }

  while (rotenc_ticks < 0) {
    countdown_decrease();
    ++rotenc_ticks;
  }

  sevenseg_display(countdown_minutes, countdown_seconds);
}

int main()
{
  setup();
  sei();
  for (;;) {
    loop();
  }
}
