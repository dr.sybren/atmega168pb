#pragma once

#include <stdint.h>

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

// flags for backlight control
#define LCD_BACKLIGHT 0x08
#define LCD_NOBACKLIGHT 0x00

#define En 0b00000100  // Enable bit
#define Rw 0b00000010  // Read/Write bit
#define Rs 0b00000001  // Register select bit

void lcd_i2c_begin(uint8_t i2c_addr, uint8_t lcd_cols, uint8_t lcd_rows, uint8_t charsize);
void lcd_i2c_clear();
void lcd_i2c_home();
void lcd_i2c_noDisplay();
void lcd_i2c_display();
void lcd_i2c_noBlink();
void lcd_i2c_blink();
void lcd_i2c_noCursor();
void lcd_i2c_cursor();
void lcd_i2c_scrollDisplayLeft();
void lcd_i2c_scrollDisplayRight();
void lcd_i2c_printLeft();
void lcd_i2c_printRight();
void lcd_i2c_leftToRight();
void lcd_i2c_rightToLeft();
void lcd_i2c_shiftIncrement();
void lcd_i2c_shiftDecrement();
void lcd_i2c_noBacklight();
void lcd_i2c_backlight();
void lcd_i2c_autoscroll();
void lcd_i2c_noAutoscroll();
void lcd_i2c_createChar(uint8_t, uint8_t[]);
void lcd_i2c_setCursor(uint8_t, uint8_t);
void lcd_i2c_command(uint8_t);
void lcd_i2c_init();

void lcd_i2c_text(const char * text);
int lcd_i2c_printf(const char *format, ...);
