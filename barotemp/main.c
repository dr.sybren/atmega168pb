/**
 * Thermometer + Barometer
 *
 * Hardware:
 *   - ATmega168pb for running this code,
 *   - HD44780 LCD display with PCF8574 I2C expander,
 *   - BMP280 barometer/thermometer in I2C mode.
 */

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include "lcd_i2c_driver.h"
#include "bmp280_driver.h"
#include "mcp9808_driver.h"
#include "i2cmaster.h"

#define LCD_I2C_ADDR 0x27
#define BARO_I2C_ADDR 0x76
#define MCP_I2C_ADDR 0x18

#define HALT() while(true)
void (*RESET)() = 0;

int main()
{
    // Minimise power consumption by configuring unused pins as output.
    PORTB = PORTC = PORTD = PORTE = 0;
    DDRB = DDRC = DDRD = DDRE = 255;
    PRR = 255 & ~(1 << PRTWI);  // turn off everything except TWI.

    i2c_init();

    // Reset the barometer, and give it time to boot up by doing LCD stuff.
    bmp280_init(BARO_I2C_ADDR);
    mcp9808_init(MCP_I2C_ADDR);

    // Initialise the display.
    lcd_i2c_begin(0x27, 16, 2, LCD_5x8DOTS);
    lcd_i2c_text("booting...");

    // Get the chip ID numbers.
    {
        uint8_t chip_id = bmp280_chip_id();
        if (chip_id != 0x58) {
            lcd_i2c_printf("Weird BMP280 %x", chip_id);
            _delay_ms(2000);
            RESET();
        }
    }
    {
        MCP9808_ID chip_id = mcp9808_chip_id();
        if (chip_id.manufacturer != 0x54 || chip_id.device != 0x04) {
            lcd_i2c_printf("Weird MCP %x:%x", chip_id.manufacturer, chip_id.device);
            _delay_ms(2000);
            RESET();
        }
    }

    bmp280_read_calibration();
    bmp280_set_ctrl_meas(BMP280_SAMPLING_X16, BMP280_SAMPLING_X16, BMP280_MODE_SLEEP);
    bmp280_set_config(BMP280_STANDBY_MS_125, BMP280_FILTER_X16);
    bmp280_set_ctrl_meas(BMP280_SAMPLING_X16, BMP280_SAMPLING_X16, BMP280_MODE_NORMAL);

    mcp9808_set_limit(MCP9808_REG_TCRIT, 26.f);
    mcp9808_set_limit(MCP9808_REG_TUPPER, 24.f);
    mcp9808_set_limit(MCP9808_REG_TLOWER, 5.0f);
    mcp9808_set_resolution(MCP9808_RES_0_0625);

    MCP9808_config mcp_config = {0};
    mcp_config.bits.t_hysteresis = MCP9808_RES_0;
    mcp_config.bits.alert_select = 1;
    mcp_config.bits.alert_polarity = 1;
    mcp_config.bits.alert_control = 1;

    _delay_ms(100);
    PORTC |= (1 << PORTC2);

    mcp9808_set_config(mcp_config);

    _delay_ms(100);

    lcd_i2c_clear();

    // Set up watchdog to wake us from the deepest sleep.
    WDTCSR |= (1<<WDCE) | (1<<WDE);
    /* Set new prescaler(time-out) value = 64K cycles (~0.5 s) */
    WDTCSR = (1<<WDIE) | (0b101<<WDP0);
    sei();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);

    Temperature temp;
    uint32_t pressure;
    MCP9808_temp mcp9808_temp;
    for(;;) {
        bmp280_read(&temp, &pressure);

        mcp_config = mcp9808_get_config();
        mcp9808_temp = mcp9808_read_temperature();

        lcd_i2c_setCursor(0, 0);
        lcd_i2c_printf("BMP: %3d.%02d C", temp.degrees, temp.cents);

        uint16_t temp_fd = mcp9808_temp.temperature;
        uint16_t temp_fc = 100 * (mcp9808_temp.temperature - temp_fd);
        lcd_i2c_setCursor(0, 1);
        lcd_i2c_printf("MCP: %3d.%02d C%c%c%c", temp_fd, temp_fc,
            (mcp9808_temp.ta_vs_tlower ? 'v' : '-'),
            (mcp9808_temp.ta_vs_tupper ? '^' : '-'),
            (mcp9808_temp.ta_vs_tcrit ? '!' : '-'));

        // Watchdog will wake us up again.
        sleep_mode();
    }
}

ISR(WDT_vect){}
