#pragma once
#include <stdint.h>

/* BMP280 Barometer & Temperature sensor.
 * This driver assumes I2C mode. */

/* Temperature in degrees and 100th of degrees celcius. */
typedef struct Temperature {
    int8_t degrees;
    int8_t cents;
} Temperature;

enum BMP280_sampling {
    BMP280_SAMPLING_NONE = 0x00,
    BMP280_SAMPLING_X1 = 0x01,
    BMP280_SAMPLING_X2 = 0x02,
    BMP280_SAMPLING_X4 = 0x03,
    BMP280_SAMPLING_X8 = 0x04,
    BMP280_SAMPLING_X16 = 0x05
};

/** Operating mode for the sensor. */
enum BMP280_operation_mode {
    BMP280_MODE_SLEEP = 0b00,
    BMP280_MODE_FORCED = 0b01,
    BMP280_MODE_NORMAL = 0b11
};

/** Standby duration in ms */
enum BMP280_standby_duration {
    BMP280_STANDBY_MS_1 = 0x00,
    BMP280_STANDBY_MS_63 = 0x01,
    BMP280_STANDBY_MS_125 = 0x02,
    BMP280_STANDBY_MS_250 = 0x03,
    BMP280_STANDBY_MS_500 = 0x04,
    BMP280_STANDBY_MS_1000 = 0x05,
    BMP280_STANDBY_MS_2000 = 0x06,
    BMP280_STANDBY_MS_4000 = 0x07
};
/** Filtering level for sensor data. */
enum BMP280_IIR_filter {
    BMP280_FILTER_OFF = 0x00,
    BMP280_FILTER_X2 = 0x01,
    BMP280_FILTER_X4 = 0x02,
    BMP280_FILTER_X8 = 0x03,
    BMP280_FILTER_X16 = 0x04
};


// Resets the barometer. Be sure to give it time to boot up before loading calibration.
void bmp280_init(uint8_t i2c_addr);

// Read the chip ID, should be 0x58.
uint8_t bmp280_chip_id();

void bmp280_read_calibration();

void bmp280_set_ctrl_meas(enum BMP280_sampling oversample_temp,
                          enum BMP280_sampling oversample_pressure,
                          enum BMP280_operation_mode mode);

void bmp280_set_config(enum BMP280_standby_duration t_standby,
                       enum BMP280_IIR_filter iir_filter);

void bmp280_read(struct Temperature *r_temp, uint32_t *r_pressure);
