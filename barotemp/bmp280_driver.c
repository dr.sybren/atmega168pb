#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <util/delay.h>

#include "bmp280_driver.h"
#include "i2cmaster.h"

static uint8_t baro_i2c_addr = 0x76;
static int32_t t_fine;

/* Calibration data */
struct bmp280_calib {
    uint16_t dig_T1;
    int16_t dig_T2;
    int16_t dig_T3;
    uint16_t dig_P1;
    int16_t dig_P2;
    int16_t dig_P3;
    int16_t dig_P4;
    int16_t dig_P5;
    int16_t dig_P6;
    int16_t dig_P7;
    int16_t dig_P8;
    int16_t dig_P9;
} bmp280_calib;


static void baro_write_register_byte(uint8_t regnum, uint8_t byte)
{
    i2c_start_wait(baro_i2c_addr, I2C_WRITE);
    i2c_write(regnum);
    i2c_write(byte);
    i2c_stop();
}

static uint16_t i2c_read_u16(bool ack)
{
    return (((uint16_t)i2c_read(true))) | (((uint16_t)i2c_read(ack)) << 8);
}

static int16_t i2c_read_s16(bool ack)
{
    return (int16_t)i2c_read_u16(ack);
}

/* Read 20 bit [19:12], [11:4], [3:0] */
static uint32_t i2c_read_u20(bool ack) {
    uint8_t msb  = i2c_read(true);
    uint8_t lsb  = i2c_read(true);
    uint8_t xlsb = i2c_read(ack);

    uint32_t value = (((uint32_t)msb) << 12) \
                   | (((uint32_t)lsb) << 4) \
                   | (xlsb >> 4);
    return value;
}

static Temperature calc_temperature(int32_t adc_T) {
    int32_t var1, var2;

    var1 = ((((adc_T>>3) - ((int32_t)bmp280_calib.dig_T1<<1))) * ((int32_t)bmp280_calib.dig_T2)) >> 11;
    var2 = (((((adc_T>>4) - ((int32_t)bmp280_calib.dig_T1)) * ((adc_T>>4) - ((int32_t)bmp280_calib.dig_T1))) >> 12) * ((int32_t)bmp280_calib.dig_T3)) >> 14;
    t_fine = var1 + var2;

    int32_t T = (t_fine * 5 + 128) >> 8;
    Temperature temp = {
        T / 100,
        T % 100
    };
    return temp;
}

// Returns pressure in Pascal.
static uint32_t calc_pressure(int32_t adc_P) {
    int32_t var1, var2;
    uint32_t p;

    var1 = (((int32_t)t_fine) >> 1) - (int32_t)64000;
    var2 = (((var1 >> 2) * (var1 >> 2)) >> 11) * ((int32_t)bmp280_calib.dig_P6);
    var2 = var2 + ((var1 * ((int32_t)bmp280_calib.dig_P5)) << 1);
    var2 = (var2 >> 2) + (((int32_t)bmp280_calib.dig_P4) << 16);
    var1 = (((bmp280_calib.dig_P3 * (((var1 >> 2) * (var1 >> 2)) >> 13)) >> 3) + ((((int32_t)bmp280_calib.dig_P2) * var1) >> 1)) >> 18;
    var1 = ((((32768 + var1)) * ((int32_t)bmp280_calib.dig_P1)) >> 15);
    if (var1 == 0) {
        return 0; // avoid exception caused by division by zero
    }
    p = (((uint32_t)(((int32_t)1048576) - adc_P) - (var2 >> 12))) * 3125;
    if (p < 0x80000000) {
        p = (p << 1) / ((uint32_t)var1);
    }
    else {
        p = (p / (uint32_t)var1) * 2;
    }
    var1 = (((int32_t)bmp280_calib.dig_P9) * ((int32_t)(((p >> 3) * (p >> 3)) >> 13))) >> 12;
    var2 = (((int32_t)(p >> 2)) * ((int32_t)bmp280_calib.dig_P8)) >> 13;
    p = (uint32_t)((int32_t)p + ((var1 + var2 + bmp280_calib.dig_P7) >> 4));

    return p;
}


void bmp280_init(uint8_t i2c_addr)
{
    baro_i2c_addr = i2c_addr;
    baro_write_register_byte(0xE0, 0xB6);
}

uint8_t bmp280_chip_id()
{
    i2c_start_wait(baro_i2c_addr, I2C_WRITE);
    i2c_write(0xD0);
    i2c_rep_start(baro_i2c_addr, I2C_READ);
    uint8_t chip_id = i2c_readNak();
    i2c_stop();
    return chip_id;
}

void bmp280_read_calibration()
{
    i2c_start_wait(baro_i2c_addr, I2C_WRITE);
    i2c_write(0x88);
    i2c_rep_start(baro_i2c_addr, I2C_READ);
    bmp280_calib.dig_T1 = i2c_read_u16(true);
    bmp280_calib.dig_T2 = i2c_read_s16(true);
    bmp280_calib.dig_T3 = i2c_read_s16(true);
    bmp280_calib.dig_P1 = i2c_read_u16(true);
    bmp280_calib.dig_P2 = i2c_read_s16(true);
    bmp280_calib.dig_P3 = i2c_read_s16(true);
    bmp280_calib.dig_P4 = i2c_read_s16(true);
    bmp280_calib.dig_P5 = i2c_read_s16(true);
    bmp280_calib.dig_P6 = i2c_read_s16(true);
    bmp280_calib.dig_P7 = i2c_read_s16(true);
    bmp280_calib.dig_P8 = i2c_read_s16(true);
    bmp280_calib.dig_P9 = i2c_read_s16(false);
    i2c_stop();
}

void bmp280_set_ctrl_meas(enum BMP280_sampling oversample_temp,
                          enum BMP280_sampling oversample_pressure,
                          enum BMP280_operation_mode mode)
{
    uint8_t baro_ctrl_meas = (oversample_temp << 5) | (oversample_pressure << 2) | mode;
    baro_write_register_byte(0xF4, baro_ctrl_meas);
}

void bmp280_set_config(enum BMP280_standby_duration t_standby,
                       enum BMP280_IIR_filter iir_filter)
{
    uint8_t baro_ctrl_meas = (t_standby << 5) | (iir_filter << 2);
    baro_write_register_byte(0xF5, baro_ctrl_meas);
}

void bmp280_read(struct Temperature *r_temp, uint32_t *r_pressure)
{
    // Read pressure & temperature in one go.
    i2c_start_wait(baro_i2c_addr, I2C_WRITE);
    i2c_write(0xF7);
    i2c_rep_start(baro_i2c_addr, I2C_READ);
    uint32_t raw_pressure = i2c_read_u20(true);  // 0xF7..F9
    uint32_t raw_temp = i2c_read_u20(false);  // 0xFA..FC
    i2c_stop();

    *r_temp = calc_temperature(raw_temp);
    *r_pressure = calc_pressure(raw_pressure);
}
