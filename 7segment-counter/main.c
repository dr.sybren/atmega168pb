#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>

// Bit order of the segments is: AFBGCPDE
const uint8_t PORTD_SECTIONS[] = {0xeb, 0x28, 0xb3, 0xba, 0x78, 0xda, 0xdb, 0xa8, 0xfb, 0xfa};
const uint8_t PORTD_SECTION_DP = 0b00000100;
volatile uint16_t show_number = 0;
volatile bool show_dp = false;

void idle()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();
}


ISR(TIMER2_COMPA_vect)
{
  static uint8_t factor = 1;
  uint8_t digit = (show_number / factor) % 10;
  bool show = false;

  switch(factor) {
    case 1:   // C0 high
      PORTC &= ~(1 << PORTC1 | 1 << PORTC4);
      PORTC |= 1 << PORTC0;
      show = true;
      break;
    case 10:  // C1 high
      PORTC &= ~(1 << PORTC0 | 1 << PORTC4);
      PORTC |= 1 << PORTC1;
      show = digit != 0 || show_number >= 100;
      break;
    case 100: // C4 high
      PORTC &= ~(1 << PORTC0 | 1 << PORTC1);
      PORTC |= 1 << PORTC4;
      show = digit != 0;
      break;
  }

  if (show) {
    uint8_t positive = PORTD_SECTIONS[digit];
    if (show_dp) positive |= PORTD_SECTION_DP;
    PORTD = ~positive;
  }
  else {
    PORTD = 255;  // everything off.
  }

  factor *= 10;
  if (factor > 100) factor = 1;
}


ISR(PCINT1_vect)
{
  static uint8_t last_pinC2 = (1 << PINC2);  // default high
  static uint8_t last_pinC5 = (1 << PINC5);  // default high

  uint8_t pinC2 = PINC & (1 << PINC2);
  uint8_t pinC5 = PINC & (1 << PINC5);

  if (last_pinC5 != pinC5 && pinC5 == 0) {  // only respond to C5 falling edge
    show_dp = !show_dp;
  }

  if (last_pinC2 != pinC2 && pinC2) {  // only respond to C2 rising edge
    uint8_t pinC3 = PINC & (1 << PINC3);
    uint16_t step = pinC5 ? 1 : 100;
    if (pinC3) show_number += step;
    else       show_number -= step;
    show_number = (show_number + 1000) % 1000;
  }

  last_pinC2 = pinC2;
  last_pinC5 = pinC5;
}


void setup()
{
  // Disable ADC
  ADCSRA = 0;

  DDRB = 0;
  PORTB = 0;

  // Set up port C for digital input with pullups
  MCUCR &= ~(1 << PUD);  // globally enable pull-ups
  DDRC = 0         // output pins
    | 1 << PORTC0
    | 1 << PORTC1
    | 1 << PORTC4
  ;
  PORTC = 0        // pullups
    | 1 << PORTC2
    | 1 << PORTC3
    | 1 << PORTC5
  ;

  DDRD = 255;
  PORTD = 255;

  // Enable timer 2 to toggle between the LED digits.
  TCCR2A = 0b10 << WGM20; // CTC mode
  TCCR2B = 0b111 << CS20; // clk/1024 -> 18 kHz clock
  OCR2A = 23;
  TIMSK2 = 1 << OCIE2A;

  // Wait until the pins read 1 before enabling PCI.
  while ((PINC & (1 << PINC2)) == 0 || (PINC & (1 << PINC3)) == 0) {
    idle();
  }

  // Enable Pin Change Interrupt
  PCICR = 1 << PCIE1;
  PCMSK1 = 0
    | 1 << PCINT8   // PC5
    | 1 << PCINT10  // PC2
  ;
}

int main()
{
  sei();
  setup();

  for (;;) idle();
}
