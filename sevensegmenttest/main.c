#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>

// Bit order of the segments is: AFBGCPDE
const uint8_t PORTD_SECTIONS[] = {0xeb, 0x28, 0xb3, 0xba, 0x78, 0xda, 0xdb, 0xa8, 0xfb, 0xfa};
const uint8_t PORTD_SECTION_DP = 0b00000100;

ISR(TIMER2_COMPA_vect)
{
  static uint8_t segment_mask = 0;
  static uint8_t show_segments = 0;

  PORTC = 0;
  segment_mask <<= 1;
  if (segment_mask == 0) {
    PORTC = 255;
    segment_mask = 1;
  }

  uint8_t positive = show_segments & segment_mask;
  PORTD = ~positive;

  static uint16_t floppers = 0;
  static uint8_t digit_to_display = 0;
  static bool show_dp = false;

  floppers++;
  if (floppers > 4000) {
    floppers = 0;
    show_dp = false;
    digit_to_display = (digit_to_display + 1) % 10;
  }
  else if (floppers == 2000) {
    show_dp = true;
  }
  else if (show_segments != 0) {
    return;
  }

  show_segments = PORTD_SECTIONS[digit_to_display];
  if (show_dp) show_segments |= PORTD_SECTION_DP;
}

void setup()
{
  DDRB = 0;
  PORTB = 0;

  DDRC = 255;
  PORTC = 0;

  DDRD = 255;
  PORTD = 255;

  TCCR2A = 0b10 << WGM20; // CTC mode
  TCCR2B = 0b011 << CS20; // clk/32 -> 576 kHz clock
  OCR2A = 144; // -> 1 KHz
  TIMSK2 = 1 << OCIE2A;
}

void loop()
{
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();
}

int main()
{
  sei();
  setup();

  for (;;) {
    loop();
  }
}
