# High order bit first
bit_order = 'AFBGCPDE'
bit_values = {}

sections = [
    'ABCDEF',  # 0
    'BC',      # 1
    'ABDEG',   # 2
    'ABCDG',   # 3
    'BCFG',    # 4
    'ACDFG',   # 5
    'ACDEFG',  # 6
    'ABC',     # 7
    'ABCDEFG', # 8
    'ABCDFG',  # 9
]

for idx, letter in enumerate(bit_order):
    bit_value = 2 ** (7 - idx)
    bit_values[letter] = bit_value

print(bit_values)

values = []
for digit, letters in enumerate(sections):
    value = sum(bit_values[letter] for letter in letters)
    # value = 0xff ^ value
    print(digit, f'00000000{bin(value)[2:]}'[-8:])
    values.append(hex(value))
print(', '.join(values))
