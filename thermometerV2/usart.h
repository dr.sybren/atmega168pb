/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void usart_setup(void);
int8_t usart_tx_buf_space(void);

void usart_tx_queue_byte(uint8_t byte);
void usart_tx_queue_string(const char *string);
void usart_tx_queue_float(float value);

bool usart_rx_buf_has_data(void);
uint8_t usart_rx_buf_pop_byte(void);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
class USART {
};

extern USART usart;

USART &operator<<(USART &usart, uint8_t byte);
USART &operator<<(USART &usart, int value);
USART &operator<<(USART &usart, const char *string);
USART &operator<<(USART &usart, float value);
#endif
