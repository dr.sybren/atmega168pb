/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "usart.h"
#include "buffer.hh"
#include "hardware.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#include <stdlib.h>
#include <string.h>

CircularBuffer tx_buffer;
CircularBuffer rx_buffer;
USART usart;

static void _synchronous_send_byte(const uint8_t data);
static bool _send_queued_byte(void);
static void _usart_tx_start(void);

void usart_setup(void)
{
  // Configure UART0
  UBRR0 = 12;           // baud rate 38400
  UCSR0A = 0            //
           | 0 << U2X0  // Double baud rate
      ;
  UCSR0B = 0              //
           | 1 << RXEN0   // Receiver enable
           | 1 << TXEN0   // Transmitter enable
           | 1 << RXCIE0  // Receiver interrupt
      ;
  UCSR0C = 0                  //
           | 0b00 << UMSEL00  // Mode select
           | 0b00 << UPM00    // Party: None
           | 0b0 << USBS0     // Stop bits: 1
           | 0b11 << UCSZ00   // Data bits: 8
           | 0b1 << UCPOL0    // Clock polarity
      ;
}

void usart_tx_queue_byte(const uint8_t byte)
{
  tx_buffer.queue_byte(byte);
  _usart_tx_start();
}

void usart_tx_queue_string(const char *string)
{
  while (*string) {
    usart_tx_queue_byte(*string);
    ++string;
  }
}

void usart_tx_queue_float(const float value)
{
  /* Build up a buffer in reverse order. */
  char buffer[10];
  char *buf_front = buffer;
  uint16_t decimals;  // variable to store the decimals
  int units;          // variable to store the units (part to left of decimal place)
  if (value < 0) {    // take care of negative numbers
    decimals = (int)(value * -100) % 100;  // make 1000 for 3 decimals etc.
    units = (int)(-1 * value);
  }
  else {  // positive numbers
    decimals = (int)(value * 100) % 100;
    units = (int)value;
  }

  *++buf_front = (decimals % 10) + '0';
  decimals /= 10;  // repeat for as many decimal places as you need
  *++buf_front = (decimals % 10) + '0';
  *++buf_front = '.';

  while (units > 0) {
    *++buf_front = (units % 10) + '0';
    units /= 10;
  }
  if (value < 0)
    *++buf_front = '-';  // unary minus sign for negative numbers

  for (; buf_front > buffer; --buf_front) {
    usart_tx_queue_byte(*buf_front);
  }
}

USART &operator<<(USART &usart, uint8_t byte)
{
  usart_tx_queue_byte(byte);
  return usart;
}
USART &operator<<(USART &usart, const char *string)
{
  usart_tx_queue_string(string);
  return usart;
}
USART &operator<<(USART &usart, float value)
{
  usart_tx_queue_float(value);
  return usart;
}
USART &operator<<(USART &usart, int value)
{
  char buffer[10];
  itoa(value, buffer, sizeof(buffer));
  return usart << buffer;
}

ISR(USART_UDRE_vect)
{
  if (!_send_queued_byte()) {
    // Transmission done, disable the interrupt.
    UCSR0B &= ~(1 << UDRIE0);
  }
}

static void _usart_tx_start()
{
  // Enable the interrupt to actually send the byte.
  UCSR0B |= (1 << UDRIE0);
}

bool _send_queued_byte(void)
{
  auto [byte_value, ok] = tx_buffer.pop_byte();

  if (!ok) {
    // Queue is empty.
    return false;
  }

  // _synchronous_send_byte(byte_value);
  UDR0 = byte_value;
  return true;
}

void _synchronous_send_byte(const uint8_t data)
{
  /* Wait for empty transmit buffer */
  while (!(UCSR0A & (1 << UDRE0)))
    ;
  /* Put data into buffer, sends the data */
  UDR0 = data;

  _delay_ms(50);
}

ISR(USART_RX_vect)
{
  /* Simple echo for now. */
  const uint8_t rx_byte = UDR0;
  sei();
  rx_buffer.queue_byte(rx_byte);
}

bool usart_rx_buf_has_data(void)
{
  return !rx_buffer.is_empty();
}

uint8_t usart_rx_buf_pop_byte(void)
{
  return rx_buffer.pop_byte().first;
}
