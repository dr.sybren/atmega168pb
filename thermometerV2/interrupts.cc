/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "interrupts.hh"

#include <avr/interrupt.h>

volatile bool interrupt_temperature_update = true;
volatile bool interrupt_usb_dtr = false;

ISR(TIMER1_COMPA_vect)
{
  interrupt_temperature_update = true;
}

ISR(TIMER2_COMPA_vect)
{
  // Just a wakeup for the main loop to do the button check.
}

ISR(PCINT1_vect)
{
  interrupt_usb_dtr = true;
}
