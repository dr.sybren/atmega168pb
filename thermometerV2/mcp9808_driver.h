/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* MCP9808 Temperature sensor. */

typedef enum MCP9808_register {
  MCP9808_REG_CONFIG = 0b0001,    /* Configuration */
  MCP9808_REG_TUPPER = 0b0010,    /* Alert Temperature Upper Boundary Trip */
  MCP9808_REG_TLOWER = 0b0011,    /* Alert Temperature Lower Boundary Trip */
  MCP9808_REG_TCRIT = 0b0100,     /* Critical Temperature Trip */
  MCP9808_REG_TEMP = 0b0101,      /* Ambient Temperature */
  MCP9808_REG_MANID = 0b0110,     /* Manufacturer ID */
  MCP9808_REG_DEVID_REV = 0b0111, /* Device ID/Revision */
  MCP9808_REG_RESOLUTION = 0b1000 /* Resolution */
} MCP9808_register;

typedef enum MCP9808_resolution {
  MCP9808_RES_0_5 = 0,    /* 0.5 °C */
  MCP9808_RES_0_25 = 1,   /* 0.25 °C */
  MCP9808_RES_0_125 = 2,  /* 0.125 °C */
  MCP9808_RES_0_0625 = 3, /* 0.0625 °C */
} MCP9808_resolution;

typedef enum MCP9808_hysteresis {
  MCP9808_HYST_0 = 0,   /* 0   °C */
  MCP9808_HYST_1_5 = 1, /* 1.5 °C */
  MCP9808_HYST_3 = 2,   /* 3   °C */
  MCP9808_HYST_6 = 3,   /* 6   °C */
} MCP9808_hysteresis;

typedef union MCP9808_config {
  struct {
    uint8_t alert_mode : 1;              /* Bit 0 */
    uint8_t alert_polarity : 1;          /* Bit 1 */
    uint8_t alert_select : 1;            /* Bit 2 */
    uint8_t alert_control : 1;           /* Bit 3 */
    uint8_t alert_status : 1;            /* Bit 4 */
    uint8_t interrupt_clear : 1;         /* Bit 5 */
    uint8_t lock_tupper_tlower : 1;      /* Bit 6 */
    uint8_t lock_tcrit : 1;              /* Bit 7 */
    uint8_t shutdown : 1;                /* Bit 8 */
    MCP9808_hysteresis t_hysteresis : 2; /* Bit 9-10 */
    uint8_t _pad : 5;                    /* Bit 11-16 */
  } bits;
  struct {
    uint8_t low;
    uint8_t high;
  } bytes;
} MCP9808_config;

typedef struct MCP9808_ID {
  uint16_t manufacturer;
  uint8_t device;
  uint8_t revision;
} MCP9808_ID;

typedef struct MCP9808_temp {
  bool ta_vs_tcrit : 1;
  bool ta_vs_tupper : 1;
  bool ta_vs_tlower : 1;
  float temperature;
} MCP9808_temp;

void mcp9808_init(uint8_t i2c_addr);
void mcp9808_set_config(MCP9808_config config);
void mcp9808_set_resolution(MCP9808_resolution resolution);

MCP9808_ID mcp9808_chip_id();
MCP9808_config mcp9808_get_config();
MCP9808_temp mcp9808_read_temperature();

void mcp9808_set_limit(MCP9808_register limit_register, float temperature);
float mcp9808_get_limit(MCP9808_register limit_register);

void mcp9808_shutdown();
void mcp9808_wakeup();

#ifdef __cplusplus
}
#endif
