/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include <math.h>
#include <stdbool.h>
#include <stdint.h>

#include "i2cmaster.h"
#include "mcp9808_driver.h"

static uint8_t mcp9808_i2c_addr = 0x18;

void mcp9808_init(uint8_t i2c_addr)
{
  mcp9808_i2c_addr = i2c_addr;
}

void mcp9808_set_config(MCP9808_config config)
{
  i2c_start_wait(mcp9808_i2c_addr, I2C_WRITE);
  i2c_write(MCP9808_REG_CONFIG);
  i2c_write(config.bytes.high);
  i2c_write(config.bytes.low);
  i2c_stop();
}

MCP9808_config mcp9808_get_config()
{
  MCP9808_config config = {0};

  i2c_start_wait(mcp9808_i2c_addr, I2C_WRITE);
  i2c_write(MCP9808_REG_CONFIG);

  i2c_rep_start(mcp9808_i2c_addr, I2C_READ);
  config.bytes.high = i2c_read(true);
  config.bytes.low = i2c_read(false);
  i2c_stop();

  return config;
}

MCP9808_ID mcp9808_chip_id()
{
  MCP9808_ID chip_id = {0};

  i2c_start_wait(mcp9808_i2c_addr, I2C_WRITE);
  i2c_write(MCP9808_REG_MANID);
  i2c_rep_start(mcp9808_i2c_addr, I2C_READ);
  chip_id.manufacturer = ((uint16_t)i2c_read(true) << 8) + i2c_read(false);

  i2c_rep_start(mcp9808_i2c_addr, I2C_WRITE);
  i2c_write(MCP9808_REG_DEVID_REV);
  i2c_rep_start(mcp9808_i2c_addr, I2C_READ);
  chip_id.device = i2c_read(true);
  chip_id.revision = i2c_read(false);
  i2c_stop();

  return chip_id;
}

MCP9808_temp mcp9808_read_temperature()
{
  i2c_start_wait(mcp9808_i2c_addr, I2C_WRITE);
  i2c_write(MCP9808_REG_TEMP);

  i2c_rep_start(mcp9808_i2c_addr, I2C_READ);
  uint8_t byte_high = i2c_read(true);
  uint8_t byte_low = i2c_read(false);
  i2c_stop();

  MCP9808_temp temp = {0};
  temp.ta_vs_tcrit = byte_high & 0x80;
  temp.ta_vs_tupper = byte_high & 0x40;
  temp.ta_vs_tlower = byte_high & 0x20;
  bool is_negative = byte_high & 0x10;

  byte_high <<= 4;  // Shift off status bits and multiply by 16.
  temp.temperature = byte_high + byte_low / 16.0f;

  if (is_negative) {
    temp.temperature = 256 - temp.temperature;
  }

  return temp;
}

void mcp9808_set_resolution(MCP9808_resolution resolution)
{
  i2c_start_wait(mcp9808_i2c_addr, I2C_WRITE);
  i2c_write(MCP9808_REG_RESOLUTION);
  i2c_write(resolution);
  i2c_stop();
}

void mcp9808_set_limit(MCP9808_register limit_register, float temperature)
{
  int16_t temp_int = temperature * 16;
  uint8_t byte_high = (temp_int >> 8) & 0x1F;
  uint8_t byte_low = temp_int & 0xFC;

  i2c_start_wait(mcp9808_i2c_addr, I2C_WRITE);
  i2c_write(limit_register);
  i2c_write(byte_high);
  i2c_write(byte_low);
  i2c_stop();
}

float mcp9808_get_limit(MCP9808_register limit_register)
{
  i2c_start_wait(mcp9808_i2c_addr, I2C_WRITE);
  i2c_write(limit_register);

  i2c_rep_start(mcp9808_i2c_addr, I2C_READ);
  uint8_t byte_high = i2c_read(true);
  uint8_t byte_low = i2c_read(false);
  i2c_stop();

  byte_high &= 0x1F;                 // Clear should-be-0 bits
  if ((byte_high & 0x10) == 0x10) {  // TA < 0°C
    byte_high &= 0x0F;               // Clear SIGN
    return 256 - (byte_high * 16 + byte_low / 16.0f);
  }
  return byte_high * 16 + byte_low / 16.0f;
}

static void _mcp9808_shutdown(bool should_shutdown)
{
  MCP9808_config config = mcp9808_get_config();
  config.bits.shutdown = should_shutdown;
  mcp9808_set_config(config);
}

void mcp9808_shutdown()
{
  _mcp9808_shutdown(true);
}

void mcp9808_wakeup()
{
  _mcp9808_shutdown(false);
}
