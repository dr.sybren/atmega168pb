# ATmega168pb + MCP9808 Thermometer
# Copyright (C) 2021 dr. Sybren A. Stüvel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
TARGET := $(notdir $(realpath .))
SOURCES := $(wildcard *.c) $(wildcard *.cc)
OBJECTS := $(patsubst %.c,%.o,$(patsubst %.cc,%.o,$(SOURCES)))
AVRDUDE := avrdude -v -c usbtiny -B8 -p m168pb -V
IPECMD := ipecmd -TPPK4 -PATmega168pb -OL -M -ORIISP -OSL2$(TEMP)/ipecmd.log

CC=avr-gcc
CXX=avr-g++
# -idirafter /usr/include is for including /usr/include/simavr/avr/avr_mcu_section.h
COMMONFLAGS=-Wall -Werror -Os -mmcu=atmega168pb \
	-Wno-unused-function \
	-idirafter /usr/include \
	-ffunction-sections -fdata-sections
CFLAGS=-std=c17 $(COMMONFLAGS)
CXXFLAGS=-std=c++17 $(COMMONFLAGS)
LDFLAGS=-Wl,--gc-sections -Wl,--print-gc-sections

flash-and-clean: flash clean

flash: flash-pickit4

$(TARGET).elf: $(OBJECTS)
	$(CXX) $(CFLAGS) $(LDFLAGS) -o $@ $^

stripped: $(TARGET).elf
	avr-strip $^
	avr-size -A $^

%.S: %.c
	$(CC) $(CFLAGS) -S -o $@ $^

%.hex: %.elf
	avr-objcopy -j .text -j .data -O ihex $< $@

hex: $(TARGET).hex

elf: $(TARGET).elf

flash-avrdude: $(TARGET).hex
	$(AVRDUDE) -U flash:w:$<:i

flash-pickit4: $(TARGET).hex
	$(IPECMD) -F$(TARGET).hex
	rm -f MPLABXLog.xml* log.[0-9]

flash-and-eeprom: $(TARGET).hex
	$(AVRDUDE) -U flash:w:$<:i -U eeprom:w:eeprom.hex:i

fuses-read:
	$(AVRDUDE)

fuses-write:
	$(AVRDUDE) -U efuse:w:0xff:m -U hfuse:w:0xdd:m -U lfuse:w:0xe2:m

fuses-default:
	$(AVRDUDE) -U efuse:w:0xf9:m -U hfuse:w:0xdf:m -U lfuse:w:0x62:m

eeprom-read:
	$(AVRDUDE) -U eeprom:r:eeprom.hex:i

eeprom-write: eeprom.hex
	$(AVRDUDE) -U eeprom:w:eeprom.hex:i

clean:
	@rm -f *.o *.elf $(TARGET).hex

.PHONY: clean fuses flash
