/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#pragma once

#include <avr/io.h>
#include <stdbool.h>

#define F_CPU 8000000

// Pinout:
// PC2: IN  USB-DTR = PCINT10
// PC3: IN  TEMP ALERT
// PD2: IN  USB-SLEEPING
// PD3: OUT USB-WAKEUP
// PD4: OUT LED-AUX
// PE0: IN  Pushbutton

static inline void led_aux(const bool enable)
{
  if (enable) {
    PORTD |= (1 << PORTD4);
  }
  else {
    PORTD &= ~(1 << PORTD4);
  }
}

// Return false iff a process has opened the serial connection.
static inline bool is_usb_dtr()
{
  return PINC & (1 << PINC2);
}

static inline bool is_pushbutton_pressed(void)
{
  return (PINE & (1 << PINE0)) == 0;
}

static inline bool is_temp_alert(void)
{
  return (PINC & (1 << PINC3)) == 0;
}
