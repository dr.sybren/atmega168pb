/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "hardware.h"
#include "interrupts.hh"
#include "mcp9808_driver.h"
#include "setup.h"
#include "sevenseg.h"
#include "thermometer.h"
#include "usart.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <util/delay.h>

volatile bool hyper_mode = false;

static void usb_show_intro(void);
static void usb_terminal_refresh(void);
static void usb_status_report();

static void set_hyper_mode(const bool new_hyper_mode);
static void sample();

static void temperature_update();
static void on_usb_dtr();
static void on_pushbutton_pressed();
static void handle_usart_rx_byte(uint8_t rx_byte);

int main()
{
  setup();
  sei();

  set_hyper_mode(false);
  if (!is_usb_dtr()) {
    usb_terminal_refresh();
  }

  bool last_pushbutton_pressed = false;
  for (;;) {
    const bool is_pressed_now = is_pushbutton_pressed();
    if (is_pressed_now && !last_pushbutton_pressed) {
      on_pushbutton_pressed();
    }
    last_pushbutton_pressed = is_pressed_now;

    if (interrupt_temperature_update)
      temperature_update();

    if (interrupt_usb_dtr)
      on_usb_dtr();

    while (usart_rx_buf_has_data()) {
      uint8_t rx_byte = usart_rx_buf_pop_byte();
      handle_usart_rx_byte(rx_byte);
    }

    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_mode();
  }
}

static void set_hyper_mode(const bool new_hyper_mode)
{
  if (new_hyper_mode) {
    OCR1A = DEFAULT_HYPER_FREQ_COUNTER;
  }
  else {
    OCR1A = DEFAULT_LOW_FREQ_COUNTER;
  }
  TCNT1 = OCR1A - (OCR1A >> 2);

  hyper_mode = new_hyper_mode;
}

static void sample()
{
  TCNT1 = 0;  // Always start periodic timer from scratch.
  sevenseg_right_dot(true);

  const float temp = thermometer_read_temp();
  usart << "Temperature: " << temp << "\r\n";

  sevenseg_display(temp);
  sevenseg_right_dot(false);
}

static void temperature_update()
{
  interrupt_temperature_update = false;
  sample();
}

static void on_usb_dtr()
{
  interrupt_usb_dtr = false;

  static bool someone_was_listening = false;
  const bool someone_is_listening = !is_usb_dtr();
  if (someone_is_listening == someone_was_listening)
    return;
  someone_was_listening = someone_is_listening;

  if (!someone_is_listening) {
    return;
  }

  usb_terminal_refresh();
}

static void usb_terminal_refresh()
{
  usb_show_intro();
  sevenseg_off();  // USB is connected, 7-segment display unnecessary.
  sample();
}

static void on_pushbutton_pressed()
{
  led_aux(true);
  if (sevenseg_enabled()) {
    sevenseg_off();
  }
  else {
    sevenseg_on();
  }
  led_aux(false);
  usb_status_report();
}

static void handle_usart_rx_byte(uint8_t rx_byte)
{
  switch (rx_byte) {
    case 'd':
      if (sevenseg_enabled()) {
        sevenseg_off();
      }
      else {
        sevenseg_on();
      }
      usb_status_report();
      break;
    case 'm':
      sample();
      break;
    case 'h':  // Toggle hyper mode
      set_hyper_mode(!hyper_mode);
      usb_status_report();
      break;
    case 'H':  // Enable hyper mode
      set_hyper_mode(true);
      usb_status_report();
      break;
    case '?':
    case '/':
    case 'L' - 'A' + 1:  // Ctrl+L
      usb_terminal_refresh();
      break;
    case 's':  // Status report
      usb_status_report();
      break;
  }
}

static void usb_show_intro(void)
{
  // Source: https://www2.ccs.neu.edu/research/gpc/VonaUtils/vona/terminal/vtansi.htm
  usart << "\033[7l";    // Disable line wrap
  usart << "\033[r";     // Set scrolling region to entire screen.
  usart << "\033[2J";    // Clear screen
  usart << "\033[?25l";  // Hide cursor
  usart << "\r---------------------------\r\n";
  usart << " \033[95mSybren\033[0m's \033[92mUSB Thermometer!\033[0m\r\n";
  usart << "---------------------------\r\n";
  usart << "d: toggle 7-segment \033[1md\033[0misplay\r\n";
  usart << "h: toggle \033[1mh\033[0myper-mode\r\n";
  usart << "m: temperature \033[1mm\033[0measurement\r\n";
  usart << "\r\n";

  usart << "\033[25;0H";  // Move cursor
  usart << "---------------------------\r\n";

  usart << "\033[8;24r";  // Set scrolling region
  usart << "\033[8;0H";   // Move cursor
}

static void usb_status_report()
{
  usart << "Status: H" << hyper_mode << "D" << sevenseg_enabled() << "\r\n";
}
