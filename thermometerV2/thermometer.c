#include "thermometer.h"
#include "hardware.h"
#include "i2cmaster.h"
#include "mcp9808_driver.h"
#include "thermometer.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <util/delay.h>

#define MCP_I2C_ADDR 0x18

void thermometer_setup()
{
  i2c_init();
  mcp9808_init(MCP_I2C_ADDR);

  MCP9808_config config = mcp9808_get_config();
  config.bits.t_hysteresis = MCP9808_HYST_1_5;

  config.bits.alert_mode = false;      // false = comparator output; true = interrupt
  config.bits.alert_polarity = false;  // active low
  config.bits.alert_select = false;    // not-critical alert output
  config.bits.alert_control = true;    // enable alert pin
  config.bits.interrupt_clear = true;  // clear any pending interrupt
  config.bits.shutdown = false;
  mcp9808_set_config(config);
  mcp9808_set_resolution(MCP9808_RES_0_0625);
  mcp9808_set_limit(MCP9808_REG_TCRIT, 50.0);
  mcp9808_set_limit(MCP9808_REG_TUPPER, 50.0);
  mcp9808_set_limit(MCP9808_REG_TLOWER, 0.0);
}

#define MEDIAN_FILTER_SIZE 3
static float measurements[MEDIAN_FILTER_SIZE];
static uint8_t num_measurements = 0;

void _fifo_push_measurement(float value);
float _median_measurement();

float thermometer_read_temp()
{
  mcp9808_wakeup();
  _delay_ms(300);

  /* Sometimes an erronous 0.0 temperature is read. */
  MCP9808_temp temp;
  do {
    temp = mcp9808_read_temperature();
  } while (temp.temperature == 0.0f);

  mcp9808_shutdown();

  _fifo_push_measurement(temp.temperature);
  return _median_measurement();
}

void _fifo_push_measurement(float value)
{
  if (num_measurements < MEDIAN_FILTER_SIZE) {
    measurements[num_measurements] = value;
    ++num_measurements;
  }
  else {
    for (int8_t i = 1; i < MEDIAN_FILTER_SIZE; ++i)
      measurements[i - 1] = measurements[i];
    measurements[MEDIAN_FILTER_SIZE - 1] = value;
  }
}

int cmp(const void *a, const void *b)
{
  const float diff = *(const float *)a - *(const float *)b;
  if (diff < 0)
    return -1;
  if (diff > 0)
    return 1;
  return 0;
}

float _median_measurement()
{
  float sorted[MEDIAN_FILTER_SIZE];
  memcpy(sorted, measurements, sizeof(sorted));
  qsort(sorted, num_measurements, sizeof(float), cmp);
  return sorted[num_measurements >> 1];
}
