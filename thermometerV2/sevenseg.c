/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "sevenseg.h"

#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#define abs(x) ((x) < 0 ? -(x) : (x))

// Bit order of the segments is: GCpDEAFB
#define G (1 << 0)
#define C (1 << 1)
#define DP (1 << 2)
#define D (1 << 3)
#define E (1 << 4)
#define A (1 << 5)
#define F (1 << 6)
#define B (1 << 7)
const uint8_t PORTB_SECTIONS[] = {
    A | B | C | D | E | F,      // 0
    B | C,                      // 1
    A | B | D | E | G,          // 2
    A | B | C | D | G,          // 3
    B | C | F | G,              // 4
    A | F | G | C | D,          // 5
    A | C | D | E | F | G,      // 6
    A | B | C,                  // 7
    A | B | C | D | E | F | G,  // 8
    A | B | C | D | F | G,      // 9

};

// Pins used by the LED segments. PORTB = sink, PORTD = source
#define PORTB_MASK 0b11111111
#define PORTD_MASK 0b11100000

typedef int16_t DeciDegree;
static volatile DeciDegree display_number = 0;      // in 1/10 degree C
static volatile DeciDegree display_number_abs = 0;  // in 1/10 degree C
static volatile uint8_t decimal_point_pos = 1;
static volatile uint8_t display_dot_mask = 0;
static volatile bool display_dot_leftmost = false;
static volatile bool display_dot_rightmost = false;
static volatile bool display_enabled = true;

static void select_digit(uint8_t digit_index)
{
  switch (digit_index) {
    case 0:
      PORTD = (PORTD & ~PORTD_MASK) | (1 << PORTD5);
      break;
    case 1:
      PORTD = (PORTD & ~PORTD_MASK) | (1 << PORTD6);
      break;
    case 2:
      PORTD = (PORTD & ~PORTD_MASK) | (1 << PORTD7);
      break;
  }
}

static uint8_t segments_for_digit(uint8_t digit_index)
{
  uint8_t digit = 0;

  switch (digit_index) {
    case 0:
      digit = display_number_abs % 10;
      break;
    case 1:
      digit = (display_number_abs / 10) % 10;
      break;
    case 2:
      if (display_number < 0) {
        return G;
      }
      if (display_number < 100) {
        return 0;
      }
      digit = (display_number_abs / 100) % 10;
      break;
  }

  uint8_t segments = PORTB_SECTIONS[digit];
  if (display_dot_mask & (1 << digit_index)) {
    segments |= DP;
  }
  return segments;
}

ISR(TIMER0_COMPA_vect)
{
  // Display one digit at a time.
  static uint8_t digit_index = 255;
  digit_index = (digit_index + 1) % 3;

  // Pull down on PORTB pins for the segment to show.
  const uint8_t positive = segments_for_digit(digit_index);

  PORTB = 255;
  select_digit(digit_index);
  PORTB = ~positive;
}

void sevenseg_setup()
{
  DDRB |= PORTB_MASK;
  PORTB = PORTB_MASK;
  DDRD |= PORTD_MASK;
  PORTD &= ~PORTD_MASK;

  // Timer for LED segments updates:
  TCCR0A = 0b10 << WGM00;  // CTC mode
  TCCR0B = 0b010 << CS00;  // clkdiv
  OCR0A = 255;
  TIFR0 = 0;  // clear all interrupt flags
  TIMSK0 = 1 << OCIE0A;
}

static void _update_display_dot_mask()
{
  display_dot_mask = 0                             //
                     | display_dot_leftmost << 3   //
                     | 1 << decimal_point_pos      //
                     | display_dot_rightmost << 0  //
      ;
}

void sevenseg_display(float number)
{
  if (number <= -10 || number >= 100.0f) {
    display_number = number;
    decimal_point_pos = 0;
  }
  else {
    display_number = number * 10;
    decimal_point_pos = 1;
  }
  display_number_abs = abs(display_number);
  _update_display_dot_mask();
}

void sevenseg_extra_dots(bool leftmost, bool rightmost)
{
  display_dot_leftmost = leftmost;
  display_dot_rightmost = rightmost;
  _update_display_dot_mask();
}

void sevenseg_left_dot(bool enable)
{
  display_dot_leftmost = enable;
  _update_display_dot_mask();
}

void sevenseg_right_dot(bool enable)
{
  display_dot_rightmost = enable;
  _update_display_dot_mask();
}

void sevenseg_on()
{
  display_enabled = true;
  TIMSK0 |= (1 << OCIE0A);
  PRR &= ~(1 << PRTIM0);
  PORTB = 255;
  PORTD &= ~PORTD_MASK;
}

void sevenseg_off()
{
  display_enabled = false;
  PRR |= (1 << PRTIM0);
  TIMSK0 &= ~(1 << OCIE0A);

  PORTB = 255;
  PORTD &= ~PORTD_MASK;
}

void sevenseg_toggle()
{
  if (display_enabled) {
    sevenseg_off();
  }
  else {
    sevenseg_on();
  }
}

bool sevenseg_enabled()
{
  return display_enabled;
}
