/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2021 Sybren A. Stüvel.
 */
#include "setup.h"
#include "hardware.h"
#include "mcp9808_driver.h"
#include "sevenseg.h"
#include "thermometer.h"
#include "usart.h"

#include <stdbool.h>

#include <avr/io.h>

#define MCP_I2C_ADDR 0x18

static void setup_timers()
{
  // Timer 1 for periodic temperature updates:
  // WGM 0b0100 for CTC mode.
  TCCR1A = 0                  //
           | (0b00 << WGM10)  // CTC mode
      ;
  TCCR1B = 0                  //
           | (0b01 << WGM12)  // CTC mode
           | (0b101)          // clk / 1024
      ;
  OCR1A = DEFAULT_LOW_FREQ_COUNTER;  // ~2 Hz interrupts
  TIFR1 = 0;                         // Clear all interrupt flags
  TIMSK1 = (1 << OCIE1A);            // Enable Interrupts

  // Timer 2 for periodic polling of pushbutton (PE0 has no pin change interrupt).
  TCCR2A = 0b10 << WGM20;   // WGM = 0b010 = CTC
  TCCR2B = 0                //
           | 0 << WGM22     //
           | 0b111 << CS20  // clock / 1024
      ;
  OCR2A = 255;  // ~2 Hz interrupts
  TIFR2 = 0;    // Clear all interrupt flags
  TIMSK2 = 1 << OCIE2A;
}

void setup()
{
  // Always disable watchdog reset at startup.
  MCUSR &= ~(1 << WDRF);
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0;

  // Set the expected clockdiv.
  CLKPR = 1 << CLKPCE;
#if F_CPU == 8000000
  CLKPR = 0b0000;
#elif F_CPU == 1000000
  CLKPR = 0b0011;
#else
#  error Unexpected value for F_CPU
#endif

  PCICR = 0;  // Disable PCINT except the ones we configure.
  GTCCR = 0;  // Disable any timer sync at boot.

  DDRB = DDRC = DDRD = DDRE = 0;      // All pins as input by default.
  PORTB = PORTC = PORTD = PORTE = 0;  // Disable all pull-ups.

  PRR = 0               //
        | (1 << PRSPI)  // SPI bus
        | (1 << PRADC)  // ADC
      ;

  // AUX LED on PD4.
  DDRD = 1 << DDD4;
  PORTD = 0 << PORTD4;

  usart_setup();

  sevenseg_setup();
  sevenseg_display(88.8);

  // Set PE0 as pulled-up input for pushbutton:
  PORTE = 1 << PORTE0;

  thermometer_setup();
  setup_timers();

  // Set PC3 / PCINT11 as pulled-up input for temp alert:
  PORTC |= (1 << PORTC3);

  // Pin PC2 / PCINT10 is USB-DTR pin.
  PCMSK1 |= (1 << PCINT10);
  PCICR |= (1 << PCIE1);

  // Wait for all the pull-up resistors to do their work.
  while ((PINE & (1 << PINE0)) == 0)
    ;
}
